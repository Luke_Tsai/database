package com.omnichat.database;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan
public class DatabaseConfig {}
