package com.omnichat.database.converter;

import com.omnichat.common.enums.user.UserRole;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserRoleConverter implements AttributeConverter<UserRole, Integer> {

  @Override
  public Integer convertToDatabaseColumn(UserRole attribute) {
    return attribute.getCode();
  }

  @Override
  public UserRole convertToEntityAttribute(Integer dbData) {
    return UserRole.fromCode(dbData).get();
  }
}
