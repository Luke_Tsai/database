package com.omnichat.database.converter;

import com.omnichat.common.enums.user.UserStatus;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserStatusConverter implements AttributeConverter<UserStatus, Integer> {

  @Override
  public Integer convertToDatabaseColumn(UserStatus attribute) {
    return attribute.getCode();
  }

  @Override
  public UserStatus convertToEntityAttribute(Integer dbData) {
    return UserStatus.fromCode(dbData).get();
  }
}
