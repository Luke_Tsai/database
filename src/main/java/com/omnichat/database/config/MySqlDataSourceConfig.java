package com.omnichat.database.config;

import com.omnichat.common.utils.YamlPropertySourceFactory;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ConditionalOnProperty(name = "mysql.core.enable", havingValue = "true")
@Configuration
@EnableTransactionManagement
@PropertySource(
    value = "classpath:database-config-${spring.profiles.active}.yml",
    factory = YamlPropertySourceFactory.class)
public class MySqlDataSourceConfig {

  @Value("${mysql.jdbcUrl}")
  private String test;

  @Bean
  public DataSource mySqlDataSource() {
    return new HikariDataSource(mySqlConfig());
  }

  @Bean
  @ConfigurationProperties("mysql")
  public HikariConfig mySqlConfig() {
    return new HikariConfig();
  }

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    vendorAdapter.setGenerateDdl(true);

    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setJpaVendorAdapter(vendorAdapter);
    /*
     * As long as entities are put in packages which their names start with "com.omnichat", they
     * will be added automatically
     */
    factory.setPackagesToScan("com.omnichat");
    factory.setDataSource(mySqlDataSource());

    return factory;
  }

  @Bean
  public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
    JpaTransactionManager txManager = new JpaTransactionManager();
    txManager.setEntityManagerFactory(entityManagerFactory);
    return txManager;
  }
}
