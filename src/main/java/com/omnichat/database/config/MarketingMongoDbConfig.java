package com.omnichat.database.config;

import com.omnichat.common.utils.YamlPropertySourceFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;

@ConditionalOnProperty(name = "mongodb.marketing.enable", havingValue = "true")
@Configuration
@PropertySource(
    value = "classpath:database-config-${spring.profiles.active}.yml",
    factory = YamlPropertySourceFactory.class)
public class MarketingMongoDbConfig {

  @Value("${mongodb.marketing.uri}")
  private String marketingMongoDbUri;

  @Bean
  public SimpleMongoClientDatabaseFactory marketingMongoDb() {
    return new SimpleMongoClientDatabaseFactory(marketingMongoDbUri);
  }

  @Bean
  public MongoTemplate marketingMongoTemplate() {
    return new MongoTemplate(marketingMongoDb());
  }
}
